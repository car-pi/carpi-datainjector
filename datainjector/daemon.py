from daemoncommons.daemon import Daemon


class DataInjectorDaemon(Daemon):
    def __init__(self):
        super().__init__('InjectD')

    def startup(self):
        pass

    def shutdown(self):
        super().shutdown()
