from daemoncommons.daemon import DaemonRunner

from datainjector.daemon import DataInjectorDaemon

if __name__ == '__main__':
    d = DaemonRunner('INJECTD_CFG', ['injectd.ini', '/etc/carpi/injectd.ini'])
    d.run(DataInjectorDaemon())
